// Array Methods - Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
	-Mutator methods are functions that "mutate" or change an array after they're created.
	- These methods manipulate the original array performing various task such as adding and removing elements.
*/

// push() - adds an element in the end of an array and return the array's length.
// SYNTAX
// arrayName.push();

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

console.log('Current array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:');
console.log(fruits);

// pop()

/*
	- Removes the last element in an array and returns the removed element.
	-Syntax
		arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);


// unshift()
/*
	- Adds one or more elements at the beginning of an array.
	Syntax:

		arrayName.unshift('elementA');
		arrayName.unshift('elementA', elementB);
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);

// shift()
/*
	- removes an element at the beginning of an array and Returns the removed element.
	- Syntax
		arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice()
/*
	-Simultaneously removes elements from a specified index number and adds element
	- Syntax
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, 'Lime');
console.log('mutated array from splice method:');
console.log(fruits);

// sort()
/*
	-Rearrange the array elements in alphanumeric order.
	-Syntax:
		arrayName.sort();
*/

const randomThings = ['cat', 'boy', 'apps', 'zoo']

randomThings.sort();
console.log('Mutated array from sort method:');
console.log(randomThings);

// reverse()
// -reverse the order of array elements
// -syntax
		// array.reverse();

randomThings.reverse();
console.log('Mutated array from reverse method:');
console.log(randomThings);

// NON_MUTATOR

// indexOf()

/*
	- Returns the index num,ber of the first matching element found in an array.
	-if no match was found, the result will be -1
	- the search process will be done from first element proceeding to the element.

	-Syntax:
		arrayName.indexOf(searchValue)
*/
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE']
let firstIndex = countries.indexOf('CAN');
console.log(`Result of indexOf method: ${firstIndex}`);

// slice()
/*
	- Portion/slice elements from an array and returns a new array.
	- Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

// Slicing off elements from a specified index to the first element

let slicedArrayA = countries.slice(4);
console.log('Result from slice method:');
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index
// Note: the last element is not included.
let slicedArrayB = countries.slice(0, 3);
console.log('Result from slice method:');
console.log(slicedArrayB)

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log('Result from slice method:');
console.log(slicedArrayC);

// forEach();

/*
	- Similar to a for loop that iterates on each array element.
	- For each item in the array, the anonymous function passed in forEach() method will be run.
	- arrayName - plural for good practice
	-parameter - singular
	- Note: it must have function.

	- Syntax:
		arrayName.forEach(function(indivElement){
			statement;
		})
*/

countries.forEach(function(country) {
	console.log(country);
});

// includes()

/*
	includes()

	- includes() method checks if the argument passed can be found in the array.
	- it returns a boolean which can be saved in a variable.
		- return true if the argument is found in the array.
		-return false if it is not.

		-Syntax:
			arrayName.includes(<argumentToFind>)
*/
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes("Mouse");

        console.log(productFound1);//returns true

        let productFound2 = products.includes("Headset");

        console.log(productFound2);//returns false
